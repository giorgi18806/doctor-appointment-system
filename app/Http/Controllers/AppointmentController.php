<?php

namespace App\Http\Controllers;

use App\Models\Time;
use App\Modles\Appointment;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    public function index()
    {
        $appointments = Appointment::latest()->where('user_id', auth()->id())->get();

        return view('admin.appointment.index', compact('appointments'));
    }

    public function create()
    {
        return view('admin.appointment.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'date' => 'required|unique:appointments,date,NULL,id,user_id,' . \Auth::id(),
            'time' => 'required',
        ]);
        $appointment = Appointment::create([
            'user_id' => auth()->id(),
            'date' => $request->date,
        ]);

        foreach($request->time as $time) {
            Time::create([
                'appointment_id' => $appointment->id,
                'time' => $time,
            ]);
        }

        return redirect()->back()->with('message', 'Appointment created for ' . $appointment->date);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function check(Request $request)
    {
        $date = $request->date;
        $appointment = Appointment::where('date', $date)
            ->where('user_id', auth()->id())
            ->first();

        if(!$appointment) {
            return redirect()->route('appointment.index')->with('errmessage', 'Appointment time not available for this date');
        }

        $times = Time::where('appointment_id', $appointment->id)->get();

        return view('admin.appointment.index', compact('times', 'appointment'));
    }

    public function updateTime(Request $request)
    {
        $appointment = Time::where('appointment_id', $request->appointmentId)->delete();
        foreach($request->time as $time) {
            Time::create([
                'appointment_id' => $request->appointmentId,
                'time' => $time,
                'status' => 0,
            ]);
        }

        return redirect()->route('appointment.index')->with('message', 'Appointment time updated');
    }
}
