<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\User;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    public function index()
    {
        $doctors = User::where('role_id', 1)->get();
        return view('admin.doctor.index', compact('doctors'));
    }

    public function create()
    {
        $roles = Role::where('name', '!=', 'patient')->get();

        return view('admin.doctor.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $this->validateData();
        $data = $request->input();

        $name = (new User)->userAvatar();

        $data['image'] = $name;
        $data['password'] = bcrypt($request->password);
        User::create($data);

        return redirect()->route('doctor.index')->with('message', 'Doctor added successfully');
    }

    public function show(User $doctor)
    {
        return view('admin.doctor.delete', compact('doctor'));
    }

    public function edit(User $doctor)
    {
        $roles = Role::where('name', '!=', 'patient')->get();
        return view('admin.doctor.edit', compact('doctor', 'roles'));
    }

    public function update(Request $request, User $doctor)
    {
        $this->validateUpdate($doctor->id);
        $data = $request->input();
        $imageName = $doctor->image;
        $doctorPassword = $doctor->password;
        if($request->hasFile('image')) {
            $imageName = (new User)->userAvatar();
            unlink(public_path('images/' . $doctor->image));
        }
        $data['image'] = $imageName;
        if($request->password) {
            $data['password'] = bcrypt($request->password);
        }else {
            $data['password'] = $doctorPassword;
        }

        $doctor->update($data);

        return redirect()->route('doctor.index')->with('message', 'Doctor updated successfully');
    }

    public function destroy(User $doctor)
    {
        if(auth()->id() == $doctor->id) {
            abort(401);
        }
        $doctorDelete = $doctor->delete();
        if($doctorDelete) {
            unlink(public_path('images/' . $doctor->image));
        }

        return redirect()->route('doctor.index')->with('message', 'Doctor deleted successfully');
    }

    public function validateData()
    {
        return request()->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|max:25',
            'gender' => 'required',
            'education' => 'required',
            'address' => 'required',
            'department' => 'required',
            'phone_number' => 'required|numeric',
            'image' => 'required|mimes:jpeg,jpg,png,svg',
            'role_id' => 'required',
            'description' => 'required',
        ]);
    }

    public function validateUpdate($id)
    {
        return request()->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'nullable|min:6|max:25',
            'gender' => 'required',
            'education' => 'required',
            'address' => 'required',
            'department' => 'required',
            'phone_number' => 'required|numeric',
            'image' => 'nullable|mimes:jpeg,jpg,png,svg',
            'role_id' => 'required',
            'description' => 'required',
        ]);
    }
}
