@include('admin.layouts.header')

<div class="page-wrap">
    <div class="main-content">
        <div class="container-fluid">
@include('admin.layouts.sidebar')

@yield('content')

        </div>
    </div>
@include('admin.layouts.footer')
