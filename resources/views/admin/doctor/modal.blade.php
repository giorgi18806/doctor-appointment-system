<!-- Modal -->
<div class="modal fade" id="exampleModal{{$doctor->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Doctor Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p><img src="{{ asset('/images') }}/{{ $doctor->image }}" width="200"></p>
                <p class="badge badge-pill badge-dark">Role: {{ $doctor->role->name }}</p>
                <p><strong>Name: </strong>{{ $doctor->name }}</p>
                <p><strong>Email: </strong>{{ $doctor->email }}</p>
                <p><strong>Address: </strong>{{ $doctor->address }}</p>
                <p><strong>Phone Number: </strong>{{ $doctor->phone_number }}</p>
                <p><strong>Department: </strong>{{ $doctor->department }}</p>
                <p><strong>Education: </strong>{{ $doctor->education }}</p>
                <p><strong>Bio: </strong>{{ $doctor->description }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
