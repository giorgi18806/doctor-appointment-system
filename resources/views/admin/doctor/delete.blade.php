@extends('admin.layouts.master')

@section('content')
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-edit bg-blue"></i>
                    <div class="d-inline">
                        <h5>Doctors</h5>
                        <span>Delete Doctor</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="../index.html"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{ route('doctor.index') }}">Doctor</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Delete</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-lg-10">

            <div class="card">
                <div class="card-header">
                    <h3>Confirm Deletion</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <img src="{{ asset('images') }}/{{ $doctor->image }}" width="120" style="border-radius: 50%;">
                        <h2 class="ml-4">{{ $doctor->name }}</h2>
                    </div>
                    <form action="{{ route('doctor.destroy', $doctor->id) }}" method="POST" class="forms-sample" autocomplete="off">
                        @csrf
                        @method('DELETE')
                        <div class="card-footer">
                            <button class="btn btn-danger mr-2">Confirm</button>
                            <a href="{{ route('doctor.index') }}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
