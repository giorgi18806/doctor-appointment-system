@extends('admin.layouts.master')

@section('content')
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-edit bg-blue"></i>
                    <div class="d-inline">
                        <h5>Doctors</h5>
                        <span>Appointment Time</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="../index.html"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{ route('doctor.index') }}">Appointment</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="container">
        @if(Session::has('message'))
            <div class="alert alert-success">{{ Session::get('message') }}</div>
        @endif
        @if(Session::has('errmessage'))
            <div class="alert alert-danger">{{ Session::get('errmessage') }}</div>
        @endif
        @foreach($errors->all() as $error)
            <div class="alert alert-danger">{{ $error }}</div>
        @endforeach
        <form action="{{ route('appointment.check') }}" method="POST">
            @csrf
            <div class="card">
                <div class="card-header justify-content-between">
                    Choose Date
                    @if(isset($appointment->date))
                        <span>Your timetable for: <strong class="text-danger">{{ $appointment->date }}</strong></span>
                    @endif
                </div>
                <div class="card-body">
                    <input type="text" class="form-control datetimepicker-input" name="date" id="datepicker" data-toggle="datetimepicker" data-target="#datepicker" autocomplete="off">
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Check</button>
                </div>
            </div>
        </form>
        @if(Route::is('appointment.check'))
            <form action="{{ route('appointment.update') }}" method="POST">
                @csrf
                <div class="card">
                    <div class="card-header justify-content-between">
                        Choose AM Time
                        <span> Check/Uncheck
                            <input type="checkbox" onclick="for(c in document.getElementsByName('time[]')) document.getElementsByName('time[]').item(c).checked=this.checked">
                        </span>
                    </div>
                    <div class="card-body">
                        <input type="hidden" name="appointmentId" value="{{ $appointment->id }}">
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <td><input type="checkbox" name="time[]" value="6am" @if(isset($times)) {{ $times->contains('time', '6am') ? 'checked' : '' }} @endif>6.00</td>
                                <td><input type="checkbox" name="time[]" value="6.20am" @if(isset($times)) {{ $times->contains('time', '6.20am') ? 'checked' : '' }} @endif>6.20</td>
                                <td><input type="checkbox" name="time[]" value="6.40am" @if(isset($times)) {{ $times->contains('time', '6.40am') ? 'checked' : '' }} @endif>6.40</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="time[]" value="7am" @if(isset($times)) {{ $times->contains('time', '7am') ? 'checked' : '' }} @endif>7.00</td>
                                <td><input type="checkbox" name="time[]" value="7.20am" @if(isset($times)) {{ $times->contains('time', '7.20am') ? 'checked' : '' }} @endif>7.20</td>
                                <td><input type="checkbox" name="time[]" value="7.40am" @if(isset($times)) {{ $times->contains('time', '7.40am') ? 'checked' : '' }} @endif>7.40</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="time[]" value="8am" @if(isset($times)) {{ $times->contains('time', '8am') ? 'checked' : '' }} @endif>8.00</td>
                                <td><input type="checkbox" name="time[]" value="8.20am" @if(isset($times)) {{ $times->contains('time', '8.20am') ? 'checked' : '' }} @endif>8.20</td>
                                <td><input type="checkbox" name="time[]" value="8.40am" @if(isset($times)) {{ $times->contains('time', '8.40am') ? 'checked' : '' }} @endif>8.40</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="time[]" value="9am" @if(isset($times)) {{ $times->contains('time', '9am') ? 'checked' : '' }} @endif>9.00</td>
                                <td><input type="checkbox" name="time[]" value="9.20am" @if(isset($times)) {{ $times->contains('time', '9.20am') ? 'checked' : '' }} @endif>9.20</td>
                                <td><input type="checkbox" name="time[]" value="9.40am" @if(isset($times)) {{ $times->contains('time', '9.40am') ? 'checked' : '' }} @endif>9.40</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="time[]" value="10.00am" @if(isset($times)) {{ $times->contains('time', '10.00am') ? 'checked' : '' }} @endif>10.00</td>
                                <td><input type="checkbox" name="time[]" value="10.20am" @if(isset($times)) {{ $times->contains('time', '10.20am') ? 'checked' : '' }} @endif>10.20</td>
                                <td><input type="checkbox" name="time[]" value="10.40am" @if(isset($times)) {{ $times->contains('time', '10.40am') ? 'checked' : '' }} @endif>10.40</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="time[]" value="11.00am" @if(isset($times)) {{ $times->contains('time', '11.00am') ? 'checked' : '' }} @endif>11.00</td>
                                <td><input type="checkbox" name="time[]" value="11.20am" @if(isset($times)) {{ $times->contains('time', '11.20am') ? 'checked' : '' }} @endif>11.20</td>
                                <td><input type="checkbox" name="time[]" value="11.40am" @if(isset($times)) {{ $times->contains('time', '11.40am') ? 'checked' : '' }} @endif>11.40</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        Choose PM Time
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <td><input type="checkbox" name="time[]" value="12.00pm" @if(isset($times)) {{ $times->contains('time', '12.00pm') ? 'checked' : '' }} @endif>12.00</td>
                                <td><input type="checkbox" name="time[]" value="12.20pm" @if(isset($times)) {{ $times->contains('time', '12.20pm') ? 'checked' : '' }} @endif>12.20</td>
                                <td><input type="checkbox" name="time[]" value="12.40pm" @if(isset($times)) {{ $times->contains('time', '12.40pm') ? 'checked' : '' }} @endif>12.40</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="time[]" value="1.00pm" @if(isset($times)) {{ $times->contains('time', '1.00pm') ? 'checked' : '' }} @endif>1.00</td>
                                <td><input type="checkbox" name="time[]" value="1.20pm" @if(isset($times)) {{ $times->contains('time', '1.20pm') ? 'checked' : '' }} @endif>1.20</td>
                                <td><input type="checkbox" name="time[]" value="1.40pm" @if(isset($times)) {{ $times->contains('time', '1.40pm') ? 'checked' : '' }} @endif>1.40</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="time[]" value="2.00pm" @if(isset($times)) {{ $times->contains('time', '2.00pm') ? 'checked' : '' }} @endif>2.00</td>
                                <td><input type="checkbox" name="time[]" value="2.20pm" @if(isset($times)) {{ $times->contains('time', '2.20pm') ? 'checked' : '' }} @endif>2.20</td>
                                <td><input type="checkbox" name="time[]" value="2.40pm" @if(isset($times)) {{ $times->contains('time', '2.40pm') ? 'checked' : '' }} @endif>2.40</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="time[]" value="3.00pm" @if(isset($times)) {{ $times->contains('time', '3.00pm') ? 'checked' : '' }} @endif>3.00</td>
                                <td><input type="checkbox" name="time[]" value="3.20pm" @if(isset($times)) {{ $times->contains('time', '3.20pm') ? 'checked' : '' }} @endif>3.20</td>
                                <td><input type="checkbox" name="time[]" value="3.40pm" @if(isset($times)) {{ $times->contains('time', '3.40pm') ? 'checked' : '' }} @endif>3.40</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="time[]" value="4.00pm" @if(isset($times)) {{ $times->contains('time', '4.00pm') ? 'checked' : '' }} @endif>4.00</td>
                                <td><input type="checkbox" name="time[]" value="4.20pm" @if(isset($times)) {{ $times->contains('time', '4.20pm') ? 'checked' : '' }} @endif>4.20</td>
                                <td><input type="checkbox" name="time[]" value="4.40pm" @if(isset($times)) {{ $times->contains('time', '4.40pm') ? 'checked' : '' }} @endif>4.40</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="time[]" value="5.00pm" @if(isset($times)) {{ $times->contains('time', '5.00pm') ? 'checked' : '' }} @endif>5.00</td>
                                <td><input type="checkbox" name="time[]" value="5.20pm" @if(isset($times)) {{ $times->contains('time', '5.20pm') ? 'checked' : '' }} @endif>5.20</td>
                                <td><input type="checkbox" name="time[]" value="5.40pm" @if(isset($times)) {{ $times->contains('time', '5.40pm') ? 'checked' : '' }} @endif>5.40</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="time[]" value="6.00pm" @if(isset($times)) {{ $times->contains('time', '6.00pm') ? 'checked' : '' }} @endif>6.00</td>
                                <td><input type="checkbox" name="time[]" value="6.20pm" @if(isset($times)) {{ $times->contains('time', '6.20pm') ? 'checked' : '' }} @endif>6.20</td>
                                <td><input type="checkbox" name="time[]" value="6.40pm" @if(isset($times)) {{ $times->contains('time', '6.40pm') ? 'checked' : '' }} @endif>6.40</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="time[]" value="7.00pm" @if(isset($times)) {{ $times->contains('time', '7.00pm') ? 'checked' : '' }} @endif>7.00</td>
                                <td><input type="checkbox" name="time[]" value="7.20pm" @if(isset($times)) {{ $times->contains('time', '7.20pm') ? 'checked' : '' }} @endif>7.20</td>
                                <td><input type="checkbox" name="time[]" value="7.40pm" @if(isset($times)) {{ $times->contains('time', '7.40pm') ? 'checked' : '' }} @endif>7.40</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="time[]" value="8.00pm" @if(isset($times)) {{ $times->contains('time', '8.00pm') ? 'checked' : '' }} @endif>8.00</td>
                                <td><input type="checkbox" name="time[]" value="8.20pm" @if(isset($times)) {{ $times->contains('time', '8.20pm') ? 'checked' : '' }} @endif>8.20</td>
                                <td><input type="checkbox" name="time[]" value="8.40pm" @if(isset($times)) {{ $times->contains('time', '8.40pm') ? 'checked' : '' }} @endif>8.40</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="time[]" value="9.00pm" @if(isset($times)) {{ $times->contains('time', '9.00pm') ? 'checked' : '' }} @endif>9.00</td>
                                <td><input type="checkbox" name="time[]" value="9.20pm" @if(isset($times)) {{ $times->contains('time', '9.20pm') ? 'checked' : '' }} @endif>9.20</td>
                                <td><input type="checkbox" name="time[]" value="9.40pm" @if(isset($times)) {{ $times->contains('time', '9.40pm') ? 'checked' : '' }} @endif>9.40</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        @else
                <div class="card">
                    <div class="card-header">
                        Your appointment time list
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <th scope="col">#</th>
                                    <th scope="col">Creator</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">View/Update</th>
                            </thead>
                            <tbody>
                                @foreach($appointments as $appointment)
                                    <tr>
                                        <th scope="row">{{ $appointment->id }}</th>
                                        <td>{{ $appointment->doctor->name }}</td>
                                        <td>{{ $appointment->date }}</td>
                                        <td>
                                            <form action="{{ route('appointment.check') }}" method="POST">
                                                @csrf
                                                <input type="hidden" name="date" value="{{ $appointment->date }}">
                                                <button type="submit" class="btn btn-primary">View/Update</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
        @endif
    </div>

    <style type="text/css">
        .card {
            font-size: 24px;
        }
        input[type="checkbox"] {
            zoom: 1.4;
        }
    </style>
@endsection
