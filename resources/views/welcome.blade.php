@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <img src="{{ asset('/banner/doctor.svg') }}" class="img-fluid">
            </div>
            <div class="col-md-7">
                <h2>Create an account & book your appointment</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Ab aliquid dicta, minus nisi odio perferendis praesentium
                    quas quos saepe temporibus? Adipisci cumque dolore, explicabo
                    fuga inventore labore magni nam nobis omnis optio pariatur
                    placeat sapiente sed tempora, unde veritatis voluptate.</p>
                <div class="mt-5">
                    <button class="btn btn-success">Register as Patient</button>
                    <button class="btn btn-secondary">Login</button>
                </div>
            </div>
        </div>
        <hr>
        <div class="card">
            <div class="card-body">
                <div class="card-header">Find Doctors</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8">
                            <input type="text" name="date" class="form-control" id="datepicker">
                        </div>
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary">Find Doctors</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="card-header">Doctors</div>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Photo</th>
                                <th>Name</th>
                                <th>Department</th>
                                <th>Book</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row">1</td>
                                <td><img src="{{ asset('/banner/doctor_user.svg') }}" width="70" style="border-radius: 50%"></td>
                                <td>Name of Doctor</td>
                                <td>Cardiologist</td>
                                <td><button class="btn btn-primary">Book Appointment</button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
